// +build gofuzz

package csv

import (
	"bytes"
)

func Fuzz(data []byte) int {
	cb := func([][]byte) bool { return true }
	p := NewReader(ReaderConf{})
	err := p.Read(bytes.NewReader(data), cb)
	if err != nil {
		return 0
	} else {
		return 1
	}
}
