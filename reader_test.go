package csv

import (
	"encoding/csv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

var allTests = []struct {
	name     string
	input    string
	comma    rune
	expected [][]string
	expErr   string
}{
	{
		name:  "super-simple",
		input: "\"aaaaa\",\"bbbbb\"\n\"ccccc\",\"ddddd\"\n",
		expected: [][]string{
			[]string{"aaaaa", "bbbbb"},
			[]string{"ccccc", "ddddd"},
		},
		expErr: "",
	},
	{
		name:  "no-newline-1",
		input: "aaaa",
		expected: [][]string{
			[]string{"aaaa"},
		},
		expErr: "",
	},
	{
		name:  "no-newline-2",
		input: "aaaa,bbbb",
		expected: [][]string{
			[]string{"aaaa", "bbbb"},
		},
		expErr: "",
	},
	{
		name:  "blank-line",
		input: "a,b\n\nc,d\n\n",
		expected: [][]string{
			{"a", "b"},
			{"c", "d"},
		},
		expErr: "",
	},
	{
		name:  "blank-line-crlf",
		input: "a,b\r\n\r\nc,d\r\n\r\n",
		expected: [][]string{
			{"a", "b"},
			{"c", "d"},
		},
		expErr: "",
	},
	{
		name:  "with-newline-line-1",
		input: "aaaa\n",
		expected: [][]string{
			[]string{"aaaa"},
		},
		expErr: "",
	},
	{
		name:  "with-newline-line-2",
		input: "aaaa,bbbb\n",
		expected: [][]string{
			[]string{"aaaa", "bbbb"},
		},
		expErr: "",
	},
	{
		name:  "simple-quoted",
		input: "a,b,c\ndasd,e,,f\n1,2,3\n",
		expected: [][]string{
			[]string{"a", "b", "c"},
			[]string{"dasd", "e", "", "f"},
			[]string{"1", "2", "3"},
		},
		expErr: "",
	},
	{
		name:     "unclosed-quote",
		input:    `"a`,
		expected: [][]string(nil),
		expErr:   "unclosed quote when EOF reached",
	},
	{
		name:     "crlf",
		input:    "a,b\r\nc,d\r\n",
		expected: [][]string{{"a", "b"}, {"c", "d"}},
		expErr:   "",
	},
	{
		name:     "cr-nolf",
		input:    "a,b\rc,d\r\n",
		expected: [][]string{{"a", "b\rc", "d"}},
		expErr:   "",
	},
	{
		name:     "cr-nolf-noeol",
		input:    "a,b\rc,\r",
		expected: [][]string{{"a", "b\rc", "\r"}},
		expErr:   "",
	},
	{
		name:     "newline-after-end-quote",
		input:    "\"a\"\n",
		expected: [][]string{{"a"}},
		expErr:   "",
	},
	{
		name:     "cr-after-end-quote",
		input:    "\"a\"\r",
		expected: [][]string{{"a"}},
		expErr:   "",
	},
	{
		name: "rfc",
		input: `#field1,field2,field3
"aaa","bb
b","ccc"
"a,a","b""bb","ccc"
zzz,yyy,xxx
`,
		expected: [][]string{
			{"#field1", "field2", "field3"},
			{"aaa", "bb\nb", "ccc"},
			{"a,a", `b"bb`, "ccc"},
			{"zzz", "yyy", "xxx"},
		},
		expErr: "",
	},
	{
		name:     "alternate-comma",
		comma:    '.',
		input:    "a.b,c\n",
		expected: [][]string{{"a", "b,c"}},
		expErr:   "",
	},
	{
		name:     "single-comma",
		input:    ",",
		expected: [][]string{make([]string, 2)},
		expErr:   "",
	},
	{
		name:     "single-comma-newline",
		input:    ",\n",
		expected: [][]string{make([]string, 2)},
		expErr:   "",
	},
	{
		name:     "overflow-cols-empty-last-col",
		input:    strings.Repeat(",", outputCap+1),
		expected: [][]string{make([]string, outputCap+2)},
		expErr:   "",
	},
	{
		name:     "overflow-cols-non-empty-last-col",
		input:    strings.Repeat(",", outputCap) + "0",
		expected: [][]string{append(make([]string, outputCap), "0")},
		expErr:   "",
	},
	{
		name:     "overflow-cols-newline-after-last-col",
		input:    strings.Repeat(",", outputCap) + "0\n",
		expected: [][]string{append(make([]string, outputCap), "0")},
		expErr:   "",
	},
	{
		name:     "overflow-cols-cr-only-after-last-col",
		input:    strings.Repeat(",", outputCap) + "\r",
		expected: [][]string{append(make([]string, outputCap), "\r")},
		expErr:   "",
	},
	{
		name:     "cr-eof",
		input:    "\r",
		expected: [][]string{{"\r"}},
	},
	{
		name:     "embedded nil",
		input:    "1,2\u0000,3\n",
		expected: [][]string{{"1", "2\u0000", "3"}},
	},
	{
		name:  "quote-in-unquoted",
		input: `a"b,c`,
		//	expected: [][]string{{`a"b`, "c"}},
		expErr: `parse error on line 1, column 1: bare " in non-quoted-field`,
	},
	{
		name:  "doublequote-in-unquoted",
		input: `a""b,c`,
		//	expected: [][]string{{`a""b`, "c"}},
		expErr: `parse error on line 1, column 1: bare " in non-quoted-field`,
	},
	{
		name:   "doublequote-in-quoted",
		input:  `"a "word","b"`,
		expErr: `parse error on line 1, column 3: extraneous or missing " in quoted-field`,
	},
	{
		name:     "cr-at-field-start-unquoted",
		input:    "\rxx",
		expected: [][]string{{"\rxx"}},
	},
	{
		name:   "cr-at-field-start-quoted",
		input:  "\r\"xx\"",
		expErr: `parse error on line 1, column 1: bare " in non-quoted-field`,
	},
}

func TestCSV(t *testing.T) {
	doTests(t, func(name string, input string, comma rune) ([][]string, error, bool) {
		conf := ReaderConf{Comma: comma}

		p := NewReader(conf)
		r := strings.NewReader(input)

		var output [][]string

		err := p.Read(r, func(data [][]byte) bool {
			var row []string
			for _, cell := range data {
				row = append(row, string(cell))
			}
			output = append(output, row)
			return true
		})
		return output, err, false
	})
}

func TestCSVStdlib(t *testing.T) {
	doTests(t, func(name string, input string, comma rune) ([][]string, error, bool) {
		switch name {
		case "cr-nolf-noeol", "cr-eof", "overflow-cols-cr-only-after-last-col":
			return nil, nil, true
		case "unclosed-quote":
			// possible reenable these later
			return nil, nil, true
		}

		r := csv.NewReader(strings.NewReader(input))
		r.FieldsPerRecord = -1
		r.LazyQuotes = false
		r.Comma = comma
		result, err := r.ReadAll()
		return result, err, false
	})
}

func doTests(t *testing.T, parse func(name string, input string, comma rune) ([][]string, error, bool)) {
	for _, test := range allTests {
		f := func(t *testing.T) {
			assert := assert.New(t)
			comma := ','
			if test.comma != rune(0) {
				comma = test.comma
			}
			output, err, skip := parse(test.name, test.input, comma)
			if skip {
				t.SkipNow()
			}
			if test.expErr == "" {
				assert.NoError(err)
			} else {
				assert.Equal(test.expErr, err.Error())
			}
			assert.Equal(test.expected, output)
		}
		t.Run(test.name, f)
	}
}
