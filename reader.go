package csv

import (
	"errors"
	"fmt"
	"io"
	"unicode/utf8"
)

type state int

const (
	inQuoted state = iota
	justClosedQuoted
	inUnquoted
	inUnquotedCr
	startOfField
	startOfFieldCr

	outputCap = 64
	bufferLen = 1024
)

type ReaderConf struct {
	Comma rune
}

var (
	DefaultConf = ReaderConf{','}
)

type Reader struct {
	buf    []byte
	output [][]byte
	conf   ReaderConf

	lastIndex int
}

func NewReader(conf ReaderConf) *Reader {
	p := &Reader{
		buf:    make([]byte, 0, bufferLen),
		output: make([][]byte, 0, outputCap),
		conf:   conf,
	}
	if p.conf.Comma == '\000' {
		p.conf.Comma = ','
	}
	return p
}

func (p *Reader) appendToCell(data []byte) {
	p.buf = append(p.buf, data...)
}

func (p *Reader) appendCrToCell() {
	p.buf = append(p.buf, '\r')
}

func (p *Reader) newCell() {
	p.output = append(p.output, p.buf[p.lastIndex:])
	p.lastIndex = len(p.buf)
}

func (p *Reader) newLine() {
	p.buf = p.buf[0:0]
	p.lastIndex = 0
	p.output = p.output[0:0]
}

func (p *Reader) Read(
	r io.Reader,
	cb func([][]byte) bool,
) error {

	st := startOfField

	line := 1
	col := 0

	readBuf := make([]byte, 1024)
	curBuf := readBuf[0:0]

	var ch rune
	var chB []byte

	var bufEOF bool

	var eof bool
	for {
		if len(curBuf) < 4 && !bufEOF {
			j := copy(readBuf, curBuf)
			k, err := r.Read(readBuf[j:])
			curBuf = readBuf[0 : k+j]
			if err != nil {
				if err == io.EOF {
					bufEOF = true
				} else {
					return err
				}
			}
		}

		if bufEOF && len(curBuf) == 0 {
			eof = true
			ch = rune(0)
			chB = nil
		} else {
			var i int
			ch, i = utf8.DecodeRune(curBuf)
			chB = curBuf[0:i] // need to copy this?
			curBuf = curBuf[i:]
		}

		switch st {
		case inQuoted:
			switch {
			case ch == '"':
				st = justClosedQuoted
			case eof:
				return errors.New("unclosed quote when EOF reached")
			default:
				p.appendToCell(chB)
			}
		case inUnquoted:
			switch {
			case ch == '"':
				return fmt.Errorf("parse error on line %d, column %d: bare \" in non-quoted-field", line, col)
			case ch == p.conf.Comma:
				p.newCell()
				st = startOfField
			case ch == '\n':
				p.newCell()
				cb(p.output)
				p.newLine()
				st = startOfField
				col = 1
				line += 1
			case ch == '\r':
				st = inUnquotedCr
			case eof:
				p.newCell()
				cb(p.output)
				return nil
			default:
				p.appendToCell(chB)
			}
		case startOfField:
			switch {
			case ch == '"':
				st = inQuoted
			case ch == p.conf.Comma:
				p.newCell()
			case ch == '\n':
				if len(p.buf) != 0 || len(p.output) > 0 {
					p.newCell()
					cb(p.output)
				}
				p.newLine()
				col = 1
				line += 1
			case ch == '\r':
				st = startOfFieldCr
			case eof:
				if len(p.buf) != 0 || len(p.output) > 0 {
					p.newCell()
					cb(p.output)
				}
				return nil
			default:
				p.appendToCell(chB)
				st = inUnquoted
			}
		case justClosedQuoted:
			switch {
			case ch == '"':
				p.appendToCell(chB)
				st = inQuoted
			case ch == p.conf.Comma:
				p.newCell()
				st = startOfField
			case ch == '\n':
				p.newCell()
				cb(p.output)
				p.newLine()
				st = startOfField
				col = 1
				line += 1
			case ch == '\r':
			case eof:
				if len(p.buf) != 0 || len(p.output) > 0 { //TODO: is this if block needed?
					p.newCell()
					cb(p.output)
					p.newLine()
				}
				return nil
			default:
				return fmt.Errorf("parse error on line %d, column %d: extraneous or missing \" in quoted-field", line, col-1)
			}
		case startOfFieldCr:
			switch {
			case ch == '"':
				return fmt.Errorf("parse error on line %d, column %d: bare \" in non-quoted-field", line, col)
			case ch == '\n':
				if len(p.buf) != 0 || len(p.output) > 0 {
					p.newCell()
					cb(p.output)
				}
				p.newLine()
				col = 1
				line += 1
				st = startOfField
			case eof:
				p.appendCrToCell()
				p.newCell()
				cb(p.output)
				return nil
			default:
				p.appendCrToCell()
				p.appendToCell(chB)
				st = inUnquoted
			}
		case inUnquotedCr:
			switch {
			case ch == '\n':
				p.newCell()
				cb(p.output)
				p.newLine()
				st = startOfField
				col = 1
				line += 1
			case eof:
				p.appendCrToCell()
				p.newCell()
				cb(p.output)
				st = startOfField
				return nil
			default:
				p.appendCrToCell()
				p.appendToCell(chB)
				st = inUnquoted
			}
		default:
			panic("no default")
		}
		col++
	}
}
