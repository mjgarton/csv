.PHONY: help fuzz
.DEFAULT_GOAL: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-28s\033[0m %s\n", $$1, $$2}'

fuzz: ## Build & run fuzz test
	go get github.com/dvyukov/go-fuzz/go-fuzz
	go get github.com/dvyukov/go-fuzz/go-fuzz-build
	go-fuzz-build gitlab.com/mjgarton/csv
	mkdir -p results_fuzz
	go-fuzz -bin=./csv-fuzz.zip -workdir=./results_fuzz/

