package csv

import (
	"strings"
	"testing"
)

func benchmarkRead(b *testing.B, input string) {
	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		p := NewReader(ReaderConf{})
		r := strings.NewReader(input)
		if err := p.Read(r, doNothing); err != nil {
			b.Fatal(err)
		}
	}
}

func doNothing(data [][]byte) bool {
	return true
}

var (
	shortUnquoted = strings.Repeat("a,b,c,d,e,f,g,h,i,j,k,l,m\n", 25)
	shortQuoted   = strings.Repeat(`"a","b","c","d","e","f","g","h","i","j","k","l","m"
`, 25)

	longUnquoted = strings.Repeat("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb,ccccccccccccccccccccccccccc,dddddddddddddddddddddddddddd,eeeeeeeeeeeeeeeeeeeeeeeeeeeeeee,fffffffffffffffffffffffffffff,gggggggggggggggggggggggg,hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh,iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii,jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj,kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk,lllllllllllllllllllllllllllllllllll,mmmmmmmmmmmmmmmmmmmmmmmmmmmmm,nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn,ooooooooooooooooooooooooooooo,ppppppppppppppppppppppppppppp,qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq,rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr,ssssssssssssssssssssssssssssss,tttttttttttttttttttttttttttttt,uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu,vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv,wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww,xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx,yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy,zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz\n", 25)

	longQuoted = strings.Repeat(`"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb","ccccccccccccccccccccccccccc","dddddddddddddddddddddddddddd","eeeeeeeeeeeeeeeeeeeeeeeeeeeeeee","fffffffffffffffffffffffffffff","gggggggggggggggggggggggg","hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh","iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj","kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk","lllllllllllllllllllllllllllllllllll","mmmmmmmmmmmmmmmmmmmmmmmmmmmmm","nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn","ooooooooooooooooooooooooooooo","ppppppppppppppppppppppppppppp","qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq","rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr","ssssssssssssssssssssssssssssss","tttttttttttttttttttttttttttttt","uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu","vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv","wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww","xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx","yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy","zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
`, 25)
)

func BenchmarkReadShortUnquoted(b *testing.B) {
	benchmarkRead(b, shortUnquoted)
}

func BenchmarkReadShortQuoted(b *testing.B) {
	benchmarkRead(b, shortQuoted)
}

func BenchmarkReadLongUnquoted(b *testing.B) {
	benchmarkRead(b, longUnquoted)
}

func BenchmarkReadLongQuoted(b *testing.B) {
	benchmarkRead(b, longQuoted)
}
