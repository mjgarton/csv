COUNT=7

function benchone() {
git checkout $1
rm ben_$1.out
for x in $(seq $COUNT); do 
  go test -bench=. >> ben_$1.out
done
}

if [ "" = "$1" ]; then
	echo "usage: ./dobench <branch to compare with master>"
	exit -1
fi


benchone master
benchone $1

benchstat ben_master.out "ben_$1.out"
